//
//  TrafficLightViewDelegate.swift
//  MVP_Example
//
//  Created by Ananth Chepuri on 01/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

protocol TrafficLightViewDelegate: NSObjectProtocol {
    func displayTrafficLight(description:(String))
}

